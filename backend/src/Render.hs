{-# LANGUAGE OverloadedStrings #-}

module Render where

import Text.Blaze.Html5 as H
import Text.Blaze.Html5 (Html)
import Text.Blaze.Html5.Attributes as A
import Text.Blaze.Renderer.Text
import qualified Data.Text.Lazy as TL
import qualified Data.Text as T
import Types
import Text.Feed.Types
import PodcastFeed

renderPodcastList :: [String] -> TL.Text
renderPodcastList = renderHtml . podcastList

podcastList :: [String] -> Html
podcastList names = docTypeHtml $ do
  H.head $ do
    H.title "Podcasts"
    link ! rel "stylesheet" ! type_ "text/css" ! href "/static/index.css"
  body (nameList names)

nameList :: [String] -> Html
nameList names = ul (mapM_ nameItem names)
  where
    nameItem name = li $ a ! href (podcastLocation name) $ (toHtml name)
    podcastLocation :: String -> AttributeValue
    podcastLocation name = "/podcasts/" `mappend` (toValue name)

renderEpisodesPage :: Feed -> TL.Text
renderEpisodesPage = renderHtml . episodesPage

episodesPage :: Feed -> Html
episodesPage feed = docTypeHtml $ do
  H.head $ do
    H.title "Episodes"
    link ! rel "stylesheet" ! type_ "text/css" ! href "/static/index.css"
  body (episodesTable podcasts)
  where
    podcasts = getPodcasts feed

episodesHeader :: Html
episodesHeader = do
  thead $ tr $ do
    td $ toHtml (T.pack "Title")
    td $ toHtml (T.pack "Description")
    td $ toHtml (T.pack "Link")

episodesTable :: [Podcast] -> Html
episodesTable items = table $ do
  episodesHeader
  tbody $ mapM_ renderEpRow items
    
renderEpRow :: Podcast -> Html
renderEpRow (Podcast title desc url) = do
  tr $ do
    td $ h2 (toHtml title)
    td $ p (toHtml desc)
    td $ a ! href (toValue url) $ toHtml ("linky" :: T.Text)
