module PodcastFeed where

import Data.Maybe
import Data.Text (Text)
import Network.HTTP.Conduit
import Text.Feed.Import
import Text.Feed.Query
import Text.Feed.Types
import Types

requestFeed :: String -> IO Feed
requestFeed url = do
  source <- simpleHttp url
  case parseFeedSource source of
    Just feed -> pure feed
    Nothing -> error "can't read feed"

getPodcasts :: Feed -> [Podcast]
getPodcasts feed = catMaybes $ map podcastItem (getFeedItems feed)

podcastItem :: Item -> Maybe Podcast
podcastItem item = Podcast 
  <$> getItemTitle item
  <*> getItemSummary item
  <*> getMp3Link item

getMp3Link :: Item -> Maybe Text
getMp3Link item = case getItemEnclosure item of
  Just (link, _, _) -> Just link
  Nothing -> Nothing
