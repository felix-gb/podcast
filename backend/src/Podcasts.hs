module Podcasts where

podcasts :: [(String, String)]
podcasts =
  [ ("eng", "http://historyofenglishpodcast.com/feed/podcast/" )
  , ("byz", "https://rss.acast.com/thehistoryofbyzantium")
  , ("pal", "http://palaeocast.libsyn.com/rss")
  , ("ast", "http://www.astronomycast.com/podcast.xml")
  , ("iot", "https://podcasts.files.bbci.co.uk/b006qykl.rss")
  ]

podcastUrl :: String -> String
podcastUrl name = case lookup name podcasts of
  Just ok -> ok
  Nothing -> error "can't find podcast"

getPodcastNames :: [String]
getPodcastNames = map (\(k, _) -> k) podcasts
