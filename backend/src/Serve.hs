{-# LANGUAGE OverloadedStrings #-}

module Serve where

import Network.Wai.Middleware.RequestLogger
import Control.Monad.IO.Class
import Web.Scotty

import Render
import PodcastFeed
import Podcasts

route :: IO ()
route = scotty 3000 $ do

  middleware logStdoutDev

  get "/podcasts/:podname" $ do
    url <- fmap podcastUrl (param "podname")
    feed <- liftIO $ requestFeed url
    html $ renderEpisodesPage feed

  get "/" $ do
    let names = getPodcastNames
    html $ renderPodcastList names

  get "/static/:path" $ do
    path <- param "path"
    file $ "./static/" ++ path
