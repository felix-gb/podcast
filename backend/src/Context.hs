module Context where

import Control.Monad.Reader

data Config = Config String
  deriving (Show)

type Context a = ReaderT Config IO a

runWithContext :: Context a -> Config -> IO a
runWithContext = runReaderT
