{-# LANGUAGE OverloadedStrings #-}

module Routes where

import Network.Wai.Middleware.RequestLogger
import Control.Monad.IO.Class
import Web.Scotty

import Model

route :: IO ()
route = scotty 3000 $ do

  middleware logStdoutDev

  get "/api/all" $ do
    eps <- liftIO numberedEps
    json eps

  get "/api/set-ep-num/:podname/:num" $ do
    podname <- param "podname"
    num <- param "num"
    liftIO $ setEpNum podname num
