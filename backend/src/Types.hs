module Types where

import Data.Text

data Podcast = Podcast
  { title :: Text
  , description :: Text
  , url :: Text
  } deriving (Show)
